# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if (len(values) == 0 or len(values) == 1):
        return None
    values.sort()
    for index in range(len(values) - 1, 0, -1):
        if (values[index - 1] < values[index]):
            return values[index - 1]


def main():
    values = [0, 9, 8, 8, 7, 10, 10]
    print(find_second_largest(values))


main()
