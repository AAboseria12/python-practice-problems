# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None
    max_value = values[0]
    for number in values:
        if (number > max_value):
            number = max_value
    return max_value
